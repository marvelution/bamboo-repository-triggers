/*
 * Copyright (c) 2017-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.bamboo.build.events;

import java.time.*;
import java.util.*;
import javax.annotation.*;
import javax.inject.*;

import com.atlassian.bamboo.build.*;
import com.atlassian.bamboo.event.*;
import com.atlassian.bamboo.plan.*;
import com.atlassian.bamboo.plan.branch.*;
import com.atlassian.bamboo.plan.cache.*;
import com.atlassian.bamboo.repository.*;
import com.atlassian.bamboo.trigger.*;
import com.atlassian.bamboo.util.*;
import com.atlassian.bamboo.utils.*;
import com.atlassian.bamboo.vcs.configuration.*;
import com.atlassian.bamboo.vcs.configuration.service.*;
import com.atlassian.bamboo.vcs.module.*;
import com.atlassian.bamboo.vcs.runtime.*;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.*;
import com.atlassian.plugin.spring.scanner.annotation.component.*;
import org.slf4j.*;

import static com.atlassian.bamboo.vcs.configuration.PartialVcsRepositoryDataImpl.*;

/**
 * {@link EventListener} that syncs branches for branch plans that have multiple repositories.
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
@BambooComponent
public class ChainBranchSynchronizer
{

	private static final Logger LOGGER = LoggerFactory.getLogger(ChainBranchSynchronizer.class);
	private final EventPublisher eventPublisher;
	private final PlanManager planManager;
	private final VcsRepositoryConfigurationService repositoryConfigurationService;
	private final VcsRepositoryManager repositoryManager;
	private final RepositoryCachingFacade repositoryCachingFacade;
	private final PlanExecutionManager planExecutionManager;
	private final BuildDetectionActionFactory buildDetectionActionFactory;
	private final NonBlockingPlanExecutionService nonBlockingPlanExecutionService;
	private final TriggerTypeManager triggerTypeManager;

	@Inject
	public ChainBranchSynchronizer(
			EventPublisher eventPublisher,
			PlanManager planManager,
			VcsRepositoryConfigurationService repositoryConfigurationService,
			VcsRepositoryManager repositoryManager,
			RepositoryCachingFacade repositoryCachingFacade,
			PlanExecutionManager planExecutionManager,
			BuildDetectionActionFactory buildDetectionActionFactory,
			NonBlockingPlanExecutionService nonBlockingPlanExecutionService,
			TriggerTypeManager triggerTypeManager)
	{
		this.eventPublisher = eventPublisher;
		this.planManager = planManager;
		this.repositoryConfigurationService = repositoryConfigurationService;
		this.repositoryManager = repositoryManager;
		this.repositoryCachingFacade = repositoryCachingFacade;
		this.planExecutionManager = planExecutionManager;
		this.buildDetectionActionFactory = buildDetectionActionFactory;
		this.nonBlockingPlanExecutionService = nonBlockingPlanExecutionService;
		this.triggerTypeManager = triggerTypeManager;
	}

	@PostConstruct
	public void register()
	{
		eventPublisher.register(this);
	}

	@PreDestroy
	public void unregister()
	{
		eventPublisher.unregister(this);
	}

	@EventListener
	public void onChainCreated(ChainCreatedEvent event)
	{
		PlanKey planKey = event.getPlanKey();
		Plan plan = planManager.getPlanByKey(planKey);
		ChainBranch chainBranch = Narrow.downTo(plan, ChainBranch.class);
		if (chainBranch != null)
		{
			planManager.setPlanSuspendedState(planKey, true);
			planExecutionManager.stopRequest(planKey);
			
			LOGGER.info("Disabling plan {} for branch sync check.", planKey);

			Iterator<PlanRepositoryDefinition> planRepositoryDefinitions = chainBranch.getPlanRepositoryDefinitions().iterator();
			PlanRepositoryDefinition defaultRepository = planRepositoryDefinitions.next();
			VcsBranch defaultBranch = defaultRepository.getBranch().getVcsBranch();

			while (planRepositoryDefinitions.hasNext())
			{
				PlanRepositoryDefinition repositoryDefinition = planRepositoryDefinitions.next();

				VcsRepositoryModuleDescriptor moduleDescriptor = repositoryManager.getVcsRepositoryModuleDescriptor(
						repositoryDefinition.getPluginKey());
				VcsBranchDetector branchDetector = moduleDescriptor.getBranchDetector();

				try
				{
					Optional<VcsBranch> openBranch = repositoryCachingFacade.getOpenBranches(branchDetector, repositoryDefinition)
							.stream()
							.filter(branch -> branch.isEqualToBranchWith(defaultBranch.getName()))
							.findFirst();

					if (openBranch.isPresent())
					{
						PartialVcsRepositoryData repositoryData = createChildWithNewBranch(repositoryDefinition, openBranch.get(),
						                                                                   moduleDescriptor.getVcsBranchConfigurator());
						LOGGER.info("Updating repository {} of {} to branch {}", repositoryData.getName(), planKey,
						            openBranch.get().getName());
						if (repositoryDefinition.isLinked())
						{
							repositoryConfigurationService.editRepository(chainBranch, repositoryDefinition.getId(), repositoryData);
						}
						else
						{
							repositoryConfigurationService.createPlanRepository(chainBranch, repositoryData);
						}
					}
				}
				catch (RepositoryException e)
				{
					LOGGER.error("Failed to synchronize branches for {}; {}", planKey, e.getMessage(), e);
				}
			}

			planManager.setPlanSuspendedState(planKey, false);
			LOGGER.info("Enabling plan {} again after branch sync check.", planKey);
			triggerInitialBuildIfNeeded(planKey);
		}
	}

	private void triggerInitialBuildIfNeeded(PlanKey planKey)
	{
		ImmutableChain chain = Narrow.to(planManager.getPlanByKey(planKey), ImmutableChain.class);
		if (SystemProperty.FIRE_INITAL_BUILD_FOR_MANUAL_STRATEGY.getValue(false) || isChainTriggeredByChangeDetection(chain))
		{
			LOGGER.info("Triggering initial build for {}", planKey);
			BuildDetectionAction buildDetectionAction = buildDetectionActionFactory.createInitialBuildDetectionAction(chain);
			CacheAwareness.withValuesOlderThanTimestampReloaded(
					() -> nonBlockingPlanExecutionService.tryToStart(chain, buildDetectionAction), Instant.now().toEpochMilli());
		}
	}

	private boolean isChainTriggeredByChangeDetection(ImmutableChain chain)
	{
		return chain.getTriggerDefinitions()
				.stream()
				.map(t -> triggerTypeManager.getTriggerDescriptor(t.getPluginKey()))
				.filter(Objects::nonNull)
				.map(TriggerModuleDescriptor::getTriggerConfigurator)
				.filter(Objects::nonNull)
				.anyMatch(tc -> tc.getRepositorySelectionMode() != TriggerConfigurator.RepositorySelectionMode.NONE);
	}
}
