/*
 * Copyright (c) 2017-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.bamboo.build.trigger;

import java.util.*;

import com.atlassian.bamboo.build.*;
import com.atlassian.bamboo.plan.cache.*;
import com.atlassian.bamboo.util.*;

/**
 * {@link BuildTriggerCondition} implementation that will only allow a build to run if it is a branch build.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class IsBranchBuildTriggerCondition
		extends AbstractBuildTriggerCondition
{

	public IsBranchBuildTriggerCondition()
	{
		super("isBranchBuild");
	}

	@Override
	protected ExecutionPreference doGetExecutionPreference(
			ImmutablePlan plan,
			Map<String, String> configuration)
	{
		if (Narrow.to(plan, ImmutableChainBranch.class) != null)
		{
			return ExecutionPreference.SHOULD_RUN;
		}
		else
		{
			return ExecutionPreference.SHOULD_STOP;
		}
	}
}
