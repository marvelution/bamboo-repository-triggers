/*
 * Copyright (c) 2017-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.bamboo.build.trigger;

import java.util.*;

import com.atlassian.bamboo.build.*;
import com.atlassian.bamboo.plan.*;
import com.atlassian.bamboo.plan.cache.*;
import com.atlassian.bamboo.v2.build.*;
import org.apache.commons.configuration.*;
import org.jetbrains.annotations.*;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
abstract class AbstractBuildTriggerCondition
		extends BaseBuildConfigurationAwarePlugin
		implements BuildTriggerCondition
{

	private static final String KEY_PREFIX = "custom.triggerCondition.";
	private static final String ENABLED_KEY_SUFFIX = ".enabled";
	final String enabledKey;

	AbstractBuildTriggerCondition(String enabledKey)
	{
		this.enabledKey = KEY_PREFIX + enabledKey + ENABLED_KEY_SUFFIX;
	}

	@NotNull
	@Override
	public Map<String, String> getConfigurationMap(@NotNull HierarchicalConfiguration configuration)
	{
		Map<String, String> result = new HashMap<>();
		result.put(enabledKey, Boolean.toString(configuration.getBoolean(enabledKey, false)));
		return result;
	}

	@NotNull
	@Override
	public ExecutionPreference getExecutionPreference(
			ImmutablePlan plan,
			Map<String, String> configuration)
	{
		if (Boolean.parseBoolean(configuration.get(enabledKey)))
		{
			return doGetExecutionPreference(plan, configuration);
		}
		return ExecutionPreference.NONE;
	}

	protected abstract ExecutionPreference doGetExecutionPreference(
			ImmutablePlan plan,
			Map<String, String> configuration);

	@Override
	protected void populateContextForView(
			@NotNull Map<String, Object> context,
			@NotNull Plan plan)
	{
		Map<String, String> configuration = plan.getBuildDefinition().getCustomConfiguration();
		context.put("triggerConditionEnabled", Boolean.valueOf(configuration.get(enabledKey)));
	}
}
