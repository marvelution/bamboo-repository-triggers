/*
 * Copyright (c) 2017-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.bamboo.build.trigger;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.bamboo.build.BuildTriggerCondition.ExecutionPreference;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.bamboo.plan.cache.ImmutableChainBranch;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;

/**
 * Tests for {@link IsBranchBuildTriggerCondition}.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class IsBranchBuildTriggerConditionTest {

	private IsBranchBuildTriggerCondition condition;

	@Before
	public void setUp() throws Exception {
		condition = new IsBranchBuildTriggerCondition();
	}

	@Test
	public void testGetExecutionPreference_Disabled_ImmutableChain() throws Exception {
		assertThat(condition.getExecutionPreference(mock(ImmutableChain.class), createConfiguration(false)),
		           is(ExecutionPreference.NONE));
	}

	@Test
	public void testGetExecutionPreference_Enabled_ImmutableChain() throws Exception {
		assertThat(condition.getExecutionPreference(mock(ImmutableChain.class), createConfiguration(true)),
		           is(ExecutionPreference.SHOULD_STOP));
	}

	@Test
	public void testGetExecutionPreference_Disabled_ImmutableChainBranch() throws Exception {
		assertThat(condition.getExecutionPreference(mock(ImmutableChainBranch.class), createConfiguration(false)),
		           is(ExecutionPreference.NONE));
	}

	@Test
	public void testGetExecutionPreference_Enabled_ImmutableChainBranch() throws Exception {
		assertThat(condition.getExecutionPreference(mock(ImmutableChainBranch.class), createConfiguration(true)),
		           is(ExecutionPreference.SHOULD_RUN));
	}

	private Map<String, String> createConfiguration(boolean enabled) {
		Map<String, String> configuration = new HashMap<>();
		configuration.put(condition.enabledKey, Boolean.toString(enabled));
		return configuration;
	}
}
