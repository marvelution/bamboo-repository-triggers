This projects features custom triggers and trigger conditions for [Atlassian Bamboo](https://www.atlassian.com/software/bamboo).

Continuous Builder
==================
<https://builds.marvelution.org/browse/MC-BRT>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
